import { movieApis, tvApis } from './apis'

export const getContentsData = (term, page, videoType) => {
  if (videoType === 'tv') {
    return tvApis.searchTvShows(term, page)
  } else {
    return movieApis.searchMovies(term, page)
  }
}

export const getGenre = (videoType) => {
  if (videoType === 'tv') {
    return tvApis.getGenre()
  } else {
    return movieApis.getGenre()
  }
}
