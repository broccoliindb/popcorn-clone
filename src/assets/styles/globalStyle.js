import { createGlobalStyle } from 'styled-components'
import reset from 'styled-reset'
import './font.css'
import './variables.css'

const globalStyle = createGlobalStyle`
  ${reset}
  * {
    box-sizing: border-box;
  },
  a {
    text-decoration: none;
    color: black;
  }
  body {
    color: var(--whiteColor);
    font-family: 'Oswald','Roboto', sans-serif;
    background-color: var(--backgroundColor)
  }
`

export default globalStyle
