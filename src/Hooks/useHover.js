import { useRef, useState, useEffect } from 'react'

const useHover = () => {
  const [value, setState] = useState(false)
  const ref = useRef(null)

  const handleMouseOver = () => {
    setState(true)
  }

  const handleMouseOut = () => {
    setState(false)
  }
  useEffect(() => {
    const node = ref.current
    if (node) {
      node.addEventListener('mouseenter', handleMouseOver)
      node.addEventListener('mouseleave', handleMouseOut)

      return () => {
        node.addEventListener('mouseenter', handleMouseOver)
        node.addEventListener('mouseleave', handleMouseOut)
      }
    }
  }, [ref.current])
  return [ref, value]
}

export default useHover
