import React, { useState, useRef, useEffect } from 'react'

export const useIntersect = (
  {
    root = null,
    rootMargin,
    threshold = 0,
    sortOptId,
    genreId,
    isTriggeredSearch,
    searchTerm,
    videoType
  },
  handler
) => {
  const [entry, updateEntry] = useState({})
  const [node, setNode] = useState(null)
  const observer = useRef(null)

  useEffect(() => {
    if (observer.current) observer.current.disconnect()

    observer.current = new IntersectionObserver(
      ([entry]) => {
        if (entry.isIntersecting) {
          handler()
        }
        updateEntry(entry)
      },
      { root, rootMargin, threshold }
    )
    const { current: currentObserver } = observer
    if (node) {
      currentObserver.observe(node)
    }
    return () => currentObserver.disconnect()
  }, [
    node,
    root,
    rootMargin,
    threshold,
    sortOptId,
    genreId,
    isTriggeredSearch,
    searchTerm,
    videoType,
    handler
  ])

  return [setNode, entry]
}
