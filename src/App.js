import React, {
  useState,
  useEffect,
  useReducer,
  createContext,
  useMemo,
  memo,
  useCallback
} from 'react'
import { HashRouter, Route } from 'react-router-dom'
import Nav from './Components/Nav'
import Header from './Components/Header'
import Top from './Components/Top'
import Contents from './Components/Contents'
import Details from './Components/Details'
import GlobalStyle from './assets/styles/globalStyle'
import styled from 'styled-components'
import { checkPropTypes } from 'prop-types'

const Container = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  position: fixed;
  top: 6.7rem;
  bottom: 0;
  left: -15rem;
  transform: ${(props) =>
    props.isHover ? 'translateX(15rem)' : 'translateX(0rem)'};
  transition: transform 0.1s linear;
  @media only screen and (max-width: 1600px) {
    width: 200%;
    left: -100vw;
    transform: ${(props) =>
      props.isHover ? 'translateX(100vw)' : 'translateX(0)'};
  }
`
const initState = {
  searchTerm: '',
  statePage: 1,
  genreId: '',
  sortOptId: '',
  isTriggeredSearch: false,
  error: null,
  videoType: 'movie',
  searchData: [],
  genres: [],
  isNotFound: false
}

export const Type = {
  SET_SEARCH_TERM: 'SET_SEARCH_TERM',
  SET_GENRE_OPTION: 'SET_GENRE_OPTION',
  SET_SORT_OPTION: 'SET_SORT_OPTION',
  SET_SEARCH_OPTIONS: 'SET_SEARCH_OPTIONS',
  SET_SEARCH_TRIGGER: 'SET_SEARCH_TRIGGER',
  SET_DATA: 'SET_DATA',
  SET_NOT_FOUND: 'SET_NOT_FOUND',
  SET_DATA_INIT: 'SET_DATA_INIT',
  SELECT_VIDEO_TYPE: 'SELECT_VIDEO_TYPE',
  SET_GENRES: 'SET_GENRES',
  SET_PAGE: 'SET_PAGE'
}
export const AppContext = createContext({
  dispatch: () => {},
  searchTerm: '',
  genreId: '',
  sortOptId: '',
  isTriggeredSearch: false,
  error: null,
  videoType: 'movie',
  searchData: [],
  genres: []
})

const reducer = (state, action) => {
  switch (action.type) {
    case Type.SET_SEARCH_TRIGGER:
      return {
        ...state,
        isTriggeredSearch: true,
        genreId: ''
      }
    case Type.SET_SEARCH_TERM:
      return {
        ...state,
        searchTerm: action.term
      }
    case Type.SET_DATA_INIT:
      return {
        ...state,
        error: action.error,
        isNotFound: false,
        searchData: [...action.results],
        isTriggeredSearch: false
      }
    case Type.SET_DATA:
      return {
        ...state,
        error: action.error,
        isNotFound: false,
        searchData: [...state.searchData, ...action.results],
        isTriggeredSearch: false
      }
    case Type.SET_NOT_FOUND:
      return {
        ...state,
        error: null,
        isNotFound: true,
        searchData: []
      }
    case Type.SET_GENRES:
      return {
        ...state,
        genres: action.genres,
        error: action.error
      }
    case Type.SET_SEARCH_OPTIONS:
      return {
        ...state
      }
    case Type.SET_GENRE_OPTION:
      return {
        ...state,
        genreId: action.genreId,
        searchTerm: action.genreId ? '' : state.searchTerm
      }
    case Type.SET_SORT_OPTION:
      return {
        ...state,
        sortOptId: action.sortOptId
      }
    case Type.SELECT_VIDEO_TYPE:
      return {
        ...state,
        videoType: action.videoType,
        genreId: ''
      }
    case Type.SET_PAGE:
      return {
        ...state,
        statePage: action.page
      }
    default:
      return state
  }
}

function App() {
  const [state, dispatch] = useReducer(reducer, initState)
  const {
    isTriggeredSearch,
    searchTerm,
    videoType,
    searchData,
    genres,
    page,
    sortOptId,
    genreId,
    isNotFound
  } = state
  const value = useMemo(() => ({ ...state, dispatch }), [
    isTriggeredSearch,
    searchTerm,
    videoType,
    genres,
    page,
    sortOptId,
    genreId,
    searchData,
    isNotFound
  ])

  const [shouldShowMenu, setShowMenu] = useState(false)

  const showMenuHandler = useCallback((shouldShowMenu) => {
    setShowMenu(shouldShowMenu)
  }, [])

  const onMenuClicked = useCallback(() => {
    setShowMenu((prev) => !prev)
  }, [])

  return (
    <div className="App">
      <AppContext.Provider value={value}>
        <HashRouter>
          <Top />
          <Header onMenuClicked={onMenuClicked} />
          <Container isHover={shouldShowMenu}>
            <Nav onHovered={showMenuHandler} onOptionSelected={onMenuClicked} />
            <Route
              path="/"
              exact
              render={(props) => <Contents {...props} />}
            ></Route>
            <Route
              path="/tv"
              exact
              render={(props) => <Contents {...props} />}
            />
          </Container>
          <Route
            path="/movie/:id"
            render={(props) => <Details videoType={videoType} {...props} />}
          />
          <Route
            path="/tv/:id"
            render={(props) => <Details videoType={videoType} {...props} />}
          />
        </HashRouter>
      </AppContext.Provider>
      <GlobalStyle />
    </div>
  )
}

export default memo(App)
