import axios from 'axios'

const MOVIE_BASE_URL = process.env.REACT_APP_MOVIE_BASE_URL
const MOVIE_API_KEY = process.env.REACT_APP_MOVIE_API_KEY

const baseApi = axios.create({
  baseURL: MOVIE_BASE_URL,
  params: {
    api_key: MOVIE_API_KEY,
    language: 'en-US'
  }
})

export const tvApis = {
  searchTvShows: (term, page) => {
    const encodedTerm = encodeURIComponent(term)
    return baseApi.get('search/tv', {
      params: {
        query: encodedTerm,
        page
      }
    })
  },
  getGenre: () => baseApi.get('genre/tv/list'),
  getDiscover: (genreId, sortOpt, page) => {
    const encodedSortOpt = encodeURIComponent(sortOpt)
    return baseApi.get('discover/tv', {
      params: {
        sort_by: encodedSortOpt,
        with_genres: genreId,
        page
      }
    })
  },
  getDetails: (id) => {
    return baseApi.get(`tv/${id}`, {
      params: {
        append_to_response: 'videos,images.casts'
      }
    })
  },
  getSeason: (id, seasonNumber) => {
    return baseApi.get(`tv/${id}/season/${seasonNumber}`, {
      params: {
        append_to_response: 'credits,videos'
      }
    })
  },
  getEpisode: (id, seasonNumber, episodeNumber) => {
    return baseApi.get(
      `tv/${id}/season/${seasonNumber}/episode/${episodeNumber}`,
      {
        params: {
          append_to_response: 'credits,videos'
        }
      }
    )
  },
  getImages: (id) => {
    return baseApi.get(`tv/${id}/images`, {
      params: {
        include_image_language: 'en,null'
      }
    })
  },
  getRecommendations: (id) => baseApi.get(`/tv/${id}/recommendations`)
}

export const movieApis = {
  searchMovies: (term, page) => {
    const encodedTerm = encodeURIComponent(term)
    return baseApi.get('search/movie', {
      params: {
        query: encodedTerm,
        page
      }
    })
  },
  getGenre: () => baseApi.get('genre/movie/list'),
  getDiscover: (genreId, sortOpt, page) => {
    const encodedSortOpt = encodeURIComponent(sortOpt)
    return baseApi.get('discover/movie', {
      params: {
        sort_by: encodedSortOpt,
        with_genres: genreId,
        page
      }
    })
  },
  getDetails: (id) => {
    return baseApi.get(`movie/${id}`, {
      params: {
        append_to_response: 'videos,images,casts'
      }
    })
  },
  getImages: (id) => {
    return baseApi.get(`movie/${id}/images`, {
      params: {
        include_image_language: 'en,null'
      }
    })
  },
  getRecommendations: (id) => baseApi.get(`/movie/${id}/recommendations`)
}

export const getMovieComposite = (id) => {
  const infos = []
  infos.push(movieApis.getDetails(id))
  infos.push(movieApis.getImages(id))
  infos.push(movieApis.getRecommendations(id))
  return Promise.all(infos)
}

export const getTvComposite = (id) => {
  const infos = []
  infos.push(tvApis.getDetails(id))
  infos.push(tvApis.getImages(id))
  infos.push(tvApis.getRecommendations(id))
  return Promise.all(infos)
}
