import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 4rem;
  width: 100%;
  height: 100%;
`
const NotFound = () => {
  return <Container>No Result Found </Container>
}

export default NotFound
