import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  background-color: red;
  opacity: 0.7;
`

const Contents = styled.div`
  opacity: 1;
`
const Title = styled.div``
const Message = styled.div``
const Stack = styled.div``
let cnt = 0
const Error = (error) => {
  return (
    <Container>
      <Contents>
        <Title></Title>
        <Message>dd</Message>
        <Stack></Stack>
      </Contents>
    </Container>
  )
}

export default Error
