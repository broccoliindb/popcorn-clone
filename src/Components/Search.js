import React, { useCallback, useRef, useEffect, useContext, memo } from 'react'
import styled from 'styled-components'
import { FAwesomeIcon, addIcon } from './FontAwesomeIcon'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import useHover from '../Hooks/useHover'
import { tvApis, movieApis } from '../apis'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import { AppContext, Type } from '../App'

addIcon(faSearch)

const Container = styled.div`
  position: absolute;
  top: 0;
  right: 0.5rem;
  bottom: 0;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  padding: 0;
  cursor: pointer;
  overflow: hidden;
`
const SearchContainer = styled.div`
  display: flex;
  transform: ${(props) =>
    props.isHover || props.isSearched ? 'none' : 'translateX(16rem)'};
  transition: transform 0.3s linear;
`
const Form = styled.form`
  display: flex;
  align-items: center;
`
const SearchBox = styled.input`
  padding: 0.5rem;
  margin-right: 1rem;
  margin-left: 0.5rem;
  width: 15rem;
  opacity: ${(props) => (props.isHover || props.isSearched ? 1 : 0)};
  transition: opacity 0.3s linear;
  border: none;
  outline: none;
`
const TermClear = styled.div`
  position: absolute;
  color: red;
  right: 2rem;
  padding: 0.3rem;
  visibility: ${(props) => (props.isTermExisted ? 'visible' : 'collapse')};
`

const Search = ({ onHover }) => {
  const {
    searchTerm,
    isTriggeredSearch,
    videoType,
    page,
    dispatch
  } = useContext(AppContext)
  const searchRef = useRef(null)
  const [hoverRef, isHovered] = useHover()

  const handleSummit = useCallback((evt) => {
    evt.preventDefault()
    if (searchRef && searchRef.current) {
      dispatch({ type: Type.SET_SEARCH_TRIGGER })
    }
  }, [])
  const handleSearch = useCallback((evt) => {
    const target = evt.target
    dispatch({ type: Type.SET_SEARCH_TERM, term: target.value })
  }, [])

  const onClearTerm = useCallback(() => {
    dispatch({ type: Type.SET_SEARCH_TRIGGER })
    dispatch({ type: Type.SET_SEARCH_TERM, term: '' })
  }, [])

  useEffect(() => {
    onHover(isHovered)
  }, [isHovered])

  return (
    <Container ref={hoverRef}>
      <SearchContainer isHover={isHovered} isSearched={searchTerm}>
        <Form onSubmit={handleSummit}>
          <FAwesomeIcon icon={faSearch} />
          <SearchBox
            ref={searchRef}
            placeholder="search"
            isHover={isHovered}
            isSearched={searchTerm}
            type="text"
            value={searchTerm}
            onChange={handleSearch}
          />
          <TermClear onClick={onClearTerm} isTermExisted={searchTerm}>
            ✘
          </TermClear>
        </Form>
      </SearchContainer>
    </Container>
  )
}

Search.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  onHover: PropTypes.func.isRequired
}

export default withRouter(memo(Search))
