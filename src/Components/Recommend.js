import React, { useEffect, useContext, useState } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { AppContext } from '../App'
import { Link } from 'react-router-dom'
import noImageSmall from '../assets/images/noImageSmall.jpg'

const SLink = styled(Link)`
  position: relative;
  text-decoration: none;
  color: black;
`
const Container = styled.div`
  background-color: rgba(255, 255, 255, 0.7);
  display: flex;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  height: 100%;
  &:hover {
    background-color: rgba(255, 255, 255, 1);
  }
`
const Poster = styled.div`
  background-image: url(${(props) => props.posterUrl});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  height: 7rem;
  width: 6rem;
`
const Contents = styled.div`
  width: 100%;
  height: 100%;
  padding: 0 0.5rem;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: flex-start;
  font-size: 0.9rem;
`
const ContentsInfo = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  text-decoration: none;
  width: 100%;
`
const Title = styled.div`
  word-break: break-all;
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  max-width: 65%;
`
const Genres = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  font-size: 0.7rem;
  word-break: break-all;
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
`
const Genre = styled.div`
  margin-right: 0.15rem;
`
const Rating = styled.div`
  font-size: 0.8rem;
`
const Overview = styled.div`
  font-size: 0.8rem;
  word-break: break-all;
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  font-family: Roboto;
  line-height: 1rem;
`

const Recommend = ({
  title,
  id,
  name,
  genre_ids,
  vote_average,
  vote_count,
  release_date,
  poster_path,
  overview
}) => {
  const { genres, videoType } = useContext(AppContext)
  const [genreNames, setGenreNames] = useState(null)
  useEffect(() => {
    if (genres && genres.length) {
      const names = []
      const rst = genres.filter((genre) => {
        for (let i = 0; i < genre_ids.length; i++) {
          if (genre.id === genre_ids[i]) {
            names.push(genre.name)
          }
        }
      })
      setGenreNames(names)
    }
  }, [genres])

  return (
    <SLink to={`/${videoType}/${id}`}>
      <Container>
        <Poster
          posterUrl={
            poster_path
              ? `https://image.tmdb.org/t/p/w185/${poster_path}`
              : noImageSmall
          }
        />
        <Contents>
          <ContentsInfo>
            <Title>{title ? title : name ? name : null}</Title>
            <Rating>
              {vote_average && `⭐ ${vote_average} ( ${vote_count})`}
            </Rating>
          </ContentsInfo>
          <Genres>
            {genreNames &&
              genreNames.map((genre, index) => (
                <Genre key={genre}>
                  {genre}
                  {genreNames.length - 1 !== index ? '/' : null}
                </Genre>
              ))}
          </Genres>
          <Overview>{overview}</Overview>
        </Contents>
      </Container>
    </SLink>
  )
}

Recommend.propTypes = {
  title: PropTypes.string,
  name: PropTypes.string,
  id: PropTypes.number.isRequired,
  genre_ids: PropTypes.array,
  vote_average: PropTypes.number,
  vote_count: PropTypes.number,
  release_date: PropTypes.string,
  poster_path: PropTypes.string,
  overview: PropTypes.string
}

export default Recommend
