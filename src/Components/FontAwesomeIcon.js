import React from 'react'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styled from 'styled-components'

export const addIcon = (icon) => {
  library.add(icon)
}

export const FAwesomeIcon = styled(FontAwesomeIcon)`
  margin-right: 0.5rem;
`
