import React, {
  useRef,
  useCallback,
  useEffect,
  useState,
  useContext,
  memo
} from 'react'
import styled, { keyframes } from 'styled-components'
import PropTypes from 'prop-types'
import Loader from './Loader'
import { AppContext, Type } from '../App'
import { getContentsData } from '../controllers'
import { tvApis, movieApis } from '../apis'
import { Link } from 'react-router-dom'
import { useIntersect } from '../Hooks/useIntersectionObserver'
import noImageSmall from '../assets/images/noImageSmall.jpg'
import NotFound from './NotFound'

const Container = styled.div`
  width: 100vw;
  height: 100%;
  overflow-y: auto;
  position: relative;
  @media only screen and (max-width: 1600px) {
    width: 100%;
  }
`
const ContentsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(13rem, max-content));
  grid-auto-rows: max-content;
  grid-gap: 3px;
  width: 100%;
  @media only screen and (max-width: 600px) {
    /* ... */
    grid-template-columns: repeat(2, 50vw);
    grid-auto-rows: max-content;
  }
  /* Small devices (portrait tablets and large phones, 600px and up) */
  @media only screen and (min-width: 600px), {
    /* ... */
    grid-template-columns: repeat(3, 33vw);
    grid-auto-rows: max-content;
  }
  /* Medium devices (landscape tablets, 768px and up) */
  @media only screen and (min-width: 768px) {
    /* ... */
    /* grid-template-columns: repeat(3, 1fr);
    grid-auto-rows: max-content; */
  }
  /* Large devices (laptops/desktops, 992px and up) */
  @media only screen and (min-width: 992px) {
    /* ... */
    grid-template-columns: repeat(auto-fit, minmax(12.6rem, max-content));
    grid-auto-rows: max-content;
    grid-gap: 3px;
    width: 100%;
  }
  /* Extra large devices (large laptops and desktops, 1200px and up) */
  @media only screen and (min-width: 1200px) {
    /* ... */
  }
`
const contentsScale = keyframes`
  from {
    transform: scale(0);
  }
  to {
    transform: scale(1);
    opacity: 1;
  }
`
const Content = styled.div`
  position: relative;
  opacity: 0;
  animation: ${contentsScale} 0.3s linear
    ${(props) => (props.index % 20) / 20 + 0.8}s forwards;
  cursor: pointer;
  color: white;
`
const BackContent = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  padding: 1rem;
  width: 100%;
  height: 100%;
  z-index: 1;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  opacity: 0;
  &:hover {
    opacity: 1;
    background-color: rgba(0, 0, 0, 0.6);
  }
`
const Title = styled.div``
const Rating = styled.div``
const Year = styled.div``
const ImgContent = styled.div`
  background-image: url(${(props) => props.posterUrl});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  height: 18.5rem;
  opacity: 1;
  &:hover {
    opacity: 0;
  }
  @media only screen and (max-width: 600px) {
    /* ... */
    height: 16.5rem;
  }
  /* Small devices (portrait tablets and large phones, 600px and up) */
  @media only screen and (min-width: 600px) {
    /* ... */
    height: 20rem;
  }
  /* Medium devices (landscape tablets, 768px and up) */
  @media only screen and (min-width: 768px) {
    /* ... */
    height: 26rem;
  }
  /* Large devices (laptops/desktops, 992px and up) */
  @media only screen and (min-width: 992px) {
    /* ... */
    height: 18.5rem;
  }
  /* Extra large devices (large laptops and desktops, 1200px and up) */
  @media only screen and (min-width: 1200px) {
    /* ... */
  }
`
const Observer = styled.div`
  width: 100;
  height: 2rem;
`

const Contents = () => {
  const {
    searchData,
    searchTerm,
    genreId,
    sortOptId,
    error,
    videoType,
    genres,
    statePage,
    isTriggeredSearch,
    isNotFound,
    dispatch
  } = useContext(AppContext)
  const page = useRef(1)
  const loader = useRef(false)
  const observerRef = useRef()
  const containerRef = useRef()

  const addPage = useCallback(() => {
    page.current++
  }, [])

  const pageInit = useCallback(() => {
    page.current = 1
  }, [])

  const getContents = useCallback(() => {
    if (videoType === 'tv') {
      return tvApis.getDiscover(genreId, sortOptId, page.current)
    } else {
      return movieApis.getDiscover(genreId, sortOptId, page.current)
    }
  }, [sortOptId, genreId, page.current, videoType])

  const getAndSetContents = useCallback(() => {
    let result
    if (page.current !== 1) {
      loader.current = true
      if (searchTerm.trim()) {
        result = getContentsData(searchTerm.trim(), page.current, videoType)
      } else {
        result = getContents()
      }
      result.then((data) => {
        const results = data.data.results
        loader.current = false
        dispatch({ type: Type.SET_DATA, results, error: null })
        addPage()
      })
      result.catch((error) => {
        loader.current = false
        dispatch({ type: Type.SET_DATA_INIT, results: [], error })
      })
    }
  }, [sortOptId, genreId, isTriggeredSearch])

  const [setNode, entry] = useIntersect(
    {
      root: containerRef.current,
      rootMargin: '0px',
      threshold: 0,
      sortOptId,
      genreId,
      isTriggeredSearch,
      searchTerm,
      videoType
    },
    getAndSetContents
  )

  const ref = useRef([])
  useEffect(() => {
    ref.current = [genreId, sortOptId]
  }, [genreId, sortOptId])

  useEffect(() => {
    if (genreId && sortOptId) {
      pageInit()
      loader.current = true
      const result = getContents(page.current)
      result.then((data) => {
        addPage()
        dispatch({ type: Type.SET_DATA_INIT, results: [] })
        setTimeout(() => {
          dispatch({ type: Type.SET_DATA_INIT, results: data.data.results })
        }, 0)
        setNode(observerRef.current)
      })
      result.catch((error) => {
        dispatch({ type: Type.SET_DATA_INIT, results: [], error })
      })
      result.finally(() => {
        loader.current = false
      })
    }
  }, [genreId, sortOptId])

  useEffect(() => {
    if (isTriggeredSearch) {
      pageInit()
      if (searchTerm.trim()) {
        loader.current = true
        const result = getContentsData(
          searchTerm.trim(),
          page.current,
          videoType
        )
        result.then((data) => {
          addPage()
          dispatch({ type: Type.SET_DATA_INIT, results: [] })
          if (!data.data.results.length) {
            dispatch({ type: Type.SET_NOT_FOUND, results: [] })
          } else {
            dispatch({ type: Type.SET_DATA_INIT, results: data.data.results })
          }
          setNode(observerRef.current)
        })
        result.catch((error) => {
          dispatch({ type: Type.SET_DATA_INIT, results: [], error })
        })
        result.finally(() => {
          loader.current = false
        })
      } else {
        dispatch({ type: Type.SET_NOT_FOUND, results: [] })
      }
    }
  }, [isTriggeredSearch, searchTerm, videoType])

  return (
    <Container>
      {isNotFound ? (
        <NotFound />
      ) : (
        <>
          <ContentsContainer>
            {searchData.map((i, index) => (
              <Link key={i.id} to={`${videoType}/${i.id}`}>
                <Content index={index}>
                  <ImgContent
                    posterUrl={
                      i.poster_path
                        ? `https://image.tmdb.org/t/p/w185/${i.poster_path}`
                        : noImageSmall
                    }
                  ></ImgContent>
                  <BackContent>
                    <Title>
                      <span>{i.title ? i.title : i.name} </span>
                    </Title>
                    <Year>
                      <span>
                        {i.release_date
                          ? i.release_date.substring(0, 4)
                          : i.first_air_date &&
                            i.first_air_date.substring(0, 4)}
                      </span>
                    </Year>
                    <Rating>
                      <span>⭐ {i.vote_average} </span>
                      <span>({i.vote_count})</span>
                    </Rating>
                  </BackContent>
                </Content>
              </Link>
            ))}
          </ContentsContainer>
          <div style={{ height: '2rem' }}>
            <Loader isLoader={loader.current} />
          </div>
          <Observer isActive={searchData.length} ref={observerRef} />
        </>
      )}
    </Container>
  )
}

export default memo(Contents)
