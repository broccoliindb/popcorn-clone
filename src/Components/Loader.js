import React, { memo } from 'react'
import styled, { keyframes } from 'styled-components'
import PropTypes from 'prop-types'

const Container = styled.div`
  visibility: ${(props) => (props.isLoader ? 'visible' : 'collapse')};
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  width: 100%;
  background-color: transparent;
`
const spinner = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`
const Spinner = styled.div`
  height: 1.5rem;
  width: 1.5rem;
  border: solid 3px red;
  border-radius: 50%;
  border-bottom-color: white;
  margin: 0.5rem;
  animation: ${spinner} 1s linear
    ${(props) => (props.isLoader ? 'infinite' : 0)};
`
const Loader = ({ isLoader }) => {
  return (
    <Container isLoader={isLoader}>
      <Spinner isLoader={isLoader} />
      <span>Loading...</span>
    </Container>
  )
}

Loader.propTypes = {
  isLoader: PropTypes.bool.isRequired
}

export default memo(Loader)
