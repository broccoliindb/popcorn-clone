import React, { useContext, useCallback, useEffect, memo, useRef } from 'react'
import { AppContext, Type } from '../App'
import styled from 'styled-components'
import { movieApis, tvApis } from '../apis'
import Loader from '../Components/Loader'
import { withRouter } from 'react-router-dom'
import { getGenre } from '../controllers'
import PropTypes from 'prop-types'
import { FAwesomeIcon, addIcon } from './FontAwesomeIcon'
import { faGripLinesVertical } from '@fortawesome/free-solid-svg-icons'
import useHover from '../Hooks/useHover'

addIcon(faGripLinesVertical)

const Icon = styled(FAwesomeIcon)`
  position: fixed;
  top: 40vh;
  left: 15rem;
  font-size: 1.5rem;
  padding: 0.1rem;
  height: 4rem;
  background-color: #0093e9;
  background-image: linear-gradient(160deg, #e90800 0%, #d09d80 100%);
  z-index: 1;
  @media only screen and (max-width: 1600px) {
    visibility: collapse;
    display: none;
  }
`
const Container = styled.div`
  width: 15rem;
  height: 100%;
  @media only screen and (max-width: 1600px) {
    width: 100%;
  }
`
const InnerContainer = styled.div`
  overflow-y: auto;
  height: 100%;
`
const Genres = styled.ul``

const Genre = styled.li`
  background-color: ${(props) => (props.selected ? 'white' : 'black')};
  color: #756d54;
  padding: 0.5rem 1rem;
  cursor: pointer;
  &:hover {
    color: #ffffff;
    background-color: #0093e9;
    background-image: linear-gradient(160deg, #e90800 0%, #d09d80 100%);
    border-left: 3px solid #ffffff;
  }
`
// :root {
//   --baseBackGroundColor: #040302;
//   --redColor: #e50914;
//   --whiteColor: #ffffff;
//   --whiteLighterColor: #f5f4f4;
//   --darkGreyColor: #756d54;
//   --blueSkyColor: #3be6d7;
//   --blackColor: #000000;
//   --whiteColor: #ffffff;
//   --darkerGreyColor: #666262;
// }

const Sorts = styled.div`
  padding: 2rem 1rem;
`
const SortTitle = styled.span`
  padding-right: 1rem;
`
const Sort = styled.option`
  /* background-color: ${(props) => (props.selected ? 'white' : 'grey')}; */
  padding: 0.5rem 1rem;
  margin: 1rem;
  cursor: pointer;
  &:hover {
    color: white;
    background-color: green;
  }
`

const sortOpts = [
  { id: 'popularity.desc', name: 'Popularity' },
  { id: 'release_date.desc', name: 'Year' },
  { id: 'original_title.desc', name: 'Title' }
]

const Nav = ({ location: { pathname }, onHovered, onOptionSelected }) => {
  const [hoverRef, isHovered] = useHover()
  const {
    dispatch,
    isTriggeredSearch,
    searchTerm,
    genres,
    genreId,
    sortOptId,
    videoType,
    page
  } = useContext(AppContext)

  const loader = useRef(false)

  const selectedGenre = useCallback(
    (id) => (e) => {
      dispatch({
        type: Type.SET_GENRE_OPTION,
        genreId: id
      })
      onOptionSelected()
    },
    []
  )

  const selectedOpt = useCallback(
    () => (e) => {
      dispatch({
        type: Type.SET_SORT_OPTION,
        sortOptId: e.target.value
      })
      onOptionSelected()
    },
    []
  )

  const getGenreList = useCallback(() => {
    loader.current = true
    const result = getGenre(videoType)
    result.then((data) => {
      const genres = data.data.genres
      dispatch({ type: Type.SET_GENRES, genres, error: null })
    })
    result.catch((error) => {
      dispatch({ type: Type.SET_GENRES, genres: [], error })
    })
    result.finally(() => {
      loader.current = false
    })
  }, [videoType])

  useEffect(() => {
    if (videoType) {
      if (
        (pathname.includes('tv') && videoType === 'tv') ||
        (!pathname.includes('tv') && videoType === 'movie')
      ) {
        getGenreList()
      } else {
        const type = pathname.includes('tv') ? 'tv' : 'movie'
        dispatch({ type: Type.SELECT_VIDEO_TYPE, videoType: type })
      }
    }
  }, [videoType, pathname])

  useEffect(() => {
    if (genres && genres.length) {
      const genreId = genres[0].id
      dispatch({
        type: Type.SET_GENRE_OPTION,
        genreId
      })
    }
  }, [genres])

  useEffect(() => {
    if (sortOpts && sortOpts.length) {
      if (!sortOptId) {
        const optId = sortOpts[0].id
        dispatch({
          type: Type.SET_SORT_OPTION,
          sortOptId: optId
        })
      }
    }
  }, [sortOpts])

  useEffect(() => {
    onHovered(isHovered)
  }, [isHovered])

  return (
    <Container ref={hoverRef}>
      <InnerContainer>
        <Genres>
          {genres.map((genre) => (
            <Genre
              key={genre.id}
              selected={genre.id === genreId}
              onClick={selectedGenre(genre.id)}
            >
              {genre.name}
            </Genre>
          ))}
        </Genres>
        <Sorts>
          <SortTitle>Sort By</SortTitle>
          <label htmlFor="sortBy" />
          <select id="sortBy" onChange={selectedOpt()}>
            {sortOpts.map((opt) => (
              <Sort key={opt.id} value={opt.id}>
                {opt.name}
              </Sort>
            ))}
          </select>
        </Sorts>
        <Icon icon={faGripLinesVertical} />
      </InnerContainer>
    </Container>
  )
}

Nav.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  onHovered: PropTypes.func.isRequired,
  onOptionSelected: PropTypes.func.isRequired
}

export default withRouter(memo(Nav))
