import React, {
  useEffect,
  useCallback,
  useState,
  useRef,
  memo,
  useContext
} from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { tvApis, movieApis, getMovieComposite, getTvComposite } from '../apis'
import Recommend from './Recommend'
import { FAwesomeIcon, addIcon } from './FontAwesomeIcon'
import {
  faCalendarAlt,
  faStopwatch,
  faStar,
  faStarHalf
} from '@fortawesome/free-solid-svg-icons'
import { AppContext } from '../App'
import Combobox from './Combobox'
import noUser from '../assets/images/noUser.jpg'

addIcon(faCalendarAlt)
addIcon(faStopwatch)
addIcon(faStar)
addIcon(faStarHalf)

const Container = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  overflow-y: auto;
`
const ImageContainer = styled.div`
  background-color: black;
  position: relative;
  width: 100%;
  height: 100vh;
  display: grid;
  grid-template-areas: 'poster content';
  grid-template-columns: 1fr 2fr;
  @media only screen and (max-width: 1600px) {
    grid-template-areas: 'poster' 'content';
    grid-template-columns: 1fr;
    grid-template-rows: 1fr 2fr;
    overflow-y: auto;
    border: 3px solid red;
  }
`
const BackDrop = styled.div`
  background-image: url(${(props) => props.backdrop});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  height: 100%;
  width: 100%;
  transition: background-image 0.24s ease-in;
  position: absolute;
  @media only screen and (max-width: 1600px) {
    position: fixed;
    z-index: 0;
    left: 0;
    right: 0;
    bottom: 0;
    top: 0;
  }
`
const Poster = styled.div`
  -webkit-mask-image: -webkit-gradient(
    linear,
    right top,
    left top,
    color-stop(1, rgba(0, 0, 0, 1)),
    color-stop(0.5, rgba(0, 0, 0, 1)),
    color-stop(0, rgba(0, 0, 0, 0))
  );
  background: url(${(props) => props.poster});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  transition: background-image 0.1s ease-in;
  grid-area: poster;
  max-width: 100%;
  height: auto;
  /* min-height: 30rem;
  min-width: 50%; */
  z-index: 1;
  @media only screen and (max-width: 1600px) {
    padding-top: 130%;
    background-size: cover;
    -webkit-mask-image: -webkit-gradient(
      linear,
      right bottom,
      right top,
      color-stop(1, rgba(0, 0, 0, 1)),
      color-stop(0.5, rgba(0, 0, 0, 1)),
      color-stop(0, rgba(0, 0, 0, 0))
    );
  }
`
const Content = styled.div`
  grid-area: content;
  width: 100%;
  height: 100vh;
  z-index: 1;
  display: grid;
  grid-template-areas: 'contentInfo moreContents';
  grid-template-columns: 2fr 1fr;
  @media only screen and (max-width: 1600px) {
    height: auto;
    grid-template-areas: 'contentInfo' 'moreContents';
    grid-template-columns: 1fr;
  }
`
const Filter = styled.div`
  backdrop-filter: brightness(25%);
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 0;
  @media only screen and (max-width: 1600px) {
    position: fixed;
    z-index: 0;
    left: 0;
    right: 0;
    bottom: 0;
    top: 0;
  }
`
const Close = styled.div`
  position: fixed;
  top: 0rem;
  right: 0;
  padding: 0.5rem;
  margin: 1rem;
  background-color: rgba(0, 0, 0, 0.5);
  border-radius: 50%;
  cursor: pointer;
  z-index: 2;
  &:hover {
    background-color: rgba(255, 255, 255, 1);
    color: black;
  }
  visibility: ${(props) => (props.showTrailer ? 'collapse' : 'visible')};
`
const Return = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  padding: 0.5rem;
  margin: 1rem;
  background-color: rgba(0, 0, 0, 0.5);
  border-radius: 50%;
  cursor: pointer;
  z-index: 2;
  visibility: ${(props) => (props.showTrailer ? 'visible' : 'collapse')};
`

const ContentInfo = styled.div`
  width: 100%;
  height: 100vh;
  /* overflow-y: auto; */
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  text-shadow: 2px 2px black;
  grid-area: contentInfo;
  padding: 0 1rem;
  @media only screen and (max-width: 1600px) {
    height: auto;
    justify-content: flex-start;
    align-items: flex-start;
  }
`
const TrailerContainer = styled.div`
  position: 'relative';
  width: 100%;
  padding-top: '56.25%';
  margin-bottom: 1rem;
`
const Youtube = styled.iframe`
  /* z-index: 3; */
  position: relative;
  width: 34rem;
  height: 20rem;
  max-width: 100%;
  max-height: calc((100vw) / (16 / 9));
  /* visibility: ${(props) => (props.showTrailer ? 'visable' : 'collapse')}; */
`
const MoreContents = styled.div`
  width: 100%;
  height: 100vh;
  overflow-y: auto;
  grid-area: moreContents;
  padding: 3rem;
  @media only screen and (max-width: 1600px) {
    padding: 1rem;
    height: 100%;
    font-size: 2rem;
    overflow-y: hidden;
  }
`
const More = styled.div`
  position: relative;
  margin: 0.5rem 0;
  height: 7rem;
`
const Title = styled.div`
  font-family: Oswald;
  font-size: 3.5rem;
  margin-bottom: 2rem;
  @media only screen and (max-width: 1600px) {
    font-size: 2rem;
    margin: 2rem 0;
  }
`
const HeaderTitle = styled.h3`
  font-family: Oswald;
  font-size: 1.5rem;
  margin-bottom: 1.5rem;
`
const Info = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 1.5rem;
  @media only screen and (max-width: 1600px) {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: repeat(2, 1fr);
  }
  div {
    &:nth-child(1) {
      grid-column: 1/3;
      grid-row: 1/1;
    }
    display: flex;
    justify-content: flex-start;
    align-items: flex-start;
  }
`
const Icon = styled(FAwesomeIcon)`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  filter: drop-shadow(0 0 5px #228dff) drop-shadow(0 0 10px #228dff)
    drop-shadow(0 0 15px #228dff) drop-shadow(0 0 20px #fff);
  -webkit-filter: drop-shadow(0 0 5px #228dff) drop-shadow(0 0 10px #228dff)
    drop-shadow(0 0 15px #228dff) drop-shadow(0 0 20px #fff);
`
const Item = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 1rem;
  position: relative;
  @media only screen and (max-width: 1600px) {
    padding: 0.5rem 0;
    font-size: 0.8rem;
  }
`
const Genre = styled.div`
  margin-right: 0.5rem;
  div {
    word-break: break-all;
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
  }
`
const Overview = styled.div`
  font-size: 1.3rem;
  opacity: 0.6;
  line-height: 1.5rem;
  font-family: Roboto;
  margin-bottom: 1rem;
  div {
    word-break: break-all;
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
  }
`
const OtherInfo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  text-shadow: 2px 2px black;
  width: 100%;
  position: relative;
`
const SeasonInfo = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: 0.5rem 0;
  width: 100%;
  @media only screen and (max-width: 1600px) {
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
    padding: 0;
  }
`
const LanguageInfo = styled.div`
  margin-bottom: 1rem;
`
const DirectorInfo = styled.div`
  margin-bottom: 1rem;
`
const CastInfo = styled.div``
const Casts = styled.div`
  padding: 1rem;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  margin-bottom: 1rem;
  width: 100%;
  @media only screen and (max-width: 1600px) {
    padding: 0.5rem 1rem 0.2rem 0;
    flex-direction: column;
  }
`
const CastContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  @media only screen and (max-width: 1600px) {
    display: grid;
    padding: 0.3rem 0.5rem;
    grid-gap: 0;
    grid-template-columns: 30% 70%;
    grid-template-rows: repeat(2, 1fr);
    div {
      &:nth-child(1) {
        grid-row: 1/3;
        grid-column: 1/1;
      }
    }
  }
`
const Avatar = styled.div`
  background-image: url(${(props) => props.avatarUrl});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  height: 5.25rem;
  width: 3.5rem;
  border-radius: 20px;
  margin-bottom: 0.3rem;
  z-index: 1;
`
const ActorName = styled.div`
  font-size: 0.9rem;
  margin-bottom: 0.3rem;
`
const Charactor = styled.div`
  font-size: 0.8rem;
  font-style: italic;
  color: rgba(255, 255, 255, 0.6);
`
const customStyle = () => {}
const getCompositeInfo = (path, id) => {
  if (path.includes('tv')) {
    return getTvComposite(id)
  } else {
    return getMovieComposite(id)
  }
}

const getDirector = (crews = []) => {
  return crews
    .filter((crew) => crew.job === 'Director')
    .map((director) => director.name)
    .join(', ')
}

const getCasts = (casts = []) => {
  const actors = casts.slice(0, 5)
  return actors.map((actor) => (
    <CastContainer key={actor.id}>
      <Avatar
        avatarUrl={
          actor.profile_path
            ? `https://image.tmdb.org/t/p/w45${actor.profile_path}`
            : noUser
        }
      />
      <ActorName>{actor.name}</ActorName>
      <Charactor>{actor.character}</Charactor>
    </CastContainer>
  ))
}

const styles = {
  bg: 'white'
}

const getTvContent = (
  details,
  genres,
  seasonInfo,
  episodeInfo,
  recommends,
  itemClickedHandler
) => {
  if (!details || !seasonInfo || !episodeInfo) return
  const video = seasonInfo.videos.results.length
    ? seasonInfo.videos.results[0]
    : details.videos.results.length
    ? details.videos.results[0]
    : null
  const crews = episodeInfo.credits.crew
  const casts = episodeInfo.credits.cast
  return (
    <Content>
      <ContentInfo>
        <Title>{details.name ? details.name : null}</Title>
        <SeasonInfo>
          <Combobox
            styles={styles}
            options={details.seasons}
            itemClickedHandler={itemClickedHandler}
            zIndex={3}
          />
          {seasonInfo && (
            <Combobox
              styles={styles}
              options={seasonInfo.episodes}
              prefix="Episode"
              itemClickedHandler={itemClickedHandler}
              zIndex={2}
            />
          )}
        </SeasonInfo>
        <Info>
          {genres && (
            <Item>
              <div>{genres}</div>
            </Item>
          )}
          {episodeInfo && episodeInfo.air_date && (
            <Item>
              <Icon icon={faCalendarAlt} />
              {episodeInfo.air_date}
            </Item>
          )}
          {episodeInfo && episodeInfo.vote_average > 0 && (
            <Item>
              <Icon icon={faStar} />
              {episodeInfo.vote_average
                ? `${episodeInfo.vote_average}/10`
                : null}
            </Item>
          )}
        </Info>
        {episodeInfo && (
          <>
            <Overview>
              <div>{episodeInfo.overview ? episodeInfo.overview : null}</div>
            </Overview>
            {video && (
              <TrailerContainer>
                <Youtube
                  src={`https://www.youtube.com/embed/${video.key}?wmode=transparent`}
                  frameborder="0"
                  allowFullScreen={true}
                />
              </TrailerContainer>
            )}
          </>
        )}
        <OtherInfo>
          <LanguageInfo>
            {details.spoken_languages && details.spoken_languages.length
              ? `Languages: `
              : null}
            {details.spoken_languages.map((lan, index) => (
              <span key={index}>
                {lan.english_name}
                {details.spoken_languages.length - 1 !== index ? ', ' : null}
              </span>
            ))}
          </LanguageInfo>
          {crews && crews.length > 0 && (
            <DirectorInfo>{`Director: ${getDirector(crews)}`}</DirectorInfo>
          )}
          {casts && casts.length > 0 && (
            <>
              <CastInfo>Casts</CastInfo>
              <Casts>{getCasts(casts)}</Casts>
            </>
          )}
        </OtherInfo>
      </ContentInfo>
      <MoreContents>
        {recommends && recommends.length > 0 && (
          <>
            <HeaderTitle>Recommendatations</HeaderTitle>
            {recommends.map((recommend) => (
              <More key={recommend.id}>
                <Recommend {...recommend} />
              </More>
            ))}
          </>
        )}
      </MoreContents>
    </Content>
  )
}

const getMovieContent = (details, genres, recommends = [], video) => {
  if (!details || !recommends.length) return
  const crews = details.casts.crew
  const casts = details.casts.cast
  return (
    <Content>
      <ContentInfo>
        <Title>{details.title ? details.title : null}</Title>
        <Info>
          {genres && (
            <Item>
              <div>{genres}</div>
            </Item>
          )}
          {details.release_date && (
            <Item>
              <Icon icon={faCalendarAlt} />
              {details.release_date}
            </Item>
          )}
          {details.runtime && (
            <Item>
              <Icon icon={faStopwatch} />
              {`${details.runtime}min`}
            </Item>
          )}
          {details.vote_average && (
            <Item>
              <Icon icon={faStar} />
              {details.vote_average ? `${details.vote_average}/10` : null}
            </Item>
          )}
        </Info>
        <Overview>
          <div>{details.overview ? details.overview : null}</div>
        </Overview>
        {video && (
          <TrailerContainer>
            <Youtube
              src={`https://www.youtube.com/embed/${video.key}?wmode=transparent`}
              frameborder="0"
              allowFullScreen={true}
            />
          </TrailerContainer>
        )}
        <OtherInfo>
          <LanguageInfo>
            {details &&
            details.spoken_languages &&
            details.spoken_languages.length
              ? `Languages: `
              : null}
            {details &&
              details.spoken_languages.map((lan, index) => (
                <span key={index}>
                  {lan.english_name}
                  {details.spoken_languages.length - 1 !== index ? ', ' : null}
                </span>
              ))}
          </LanguageInfo>
          {crews && crews.length > 0 && (
            <DirectorInfo>{`Director: ${getDirector(crews)}`}</DirectorInfo>
          )}
          {casts && casts.length > 0 && (
            <>
              <CastInfo>Casts</CastInfo>
              <Casts>{getCasts(casts)}</Casts>
            </>
          )}
        </OtherInfo>
      </ContentInfo>
      <MoreContents>
        {recommends && recommends.length > 0 && (
          <>
            <HeaderTitle>Recommendatations</HeaderTitle>
            {recommends.map((recommend) => (
              <More key={recommend.id}>
                <Recommend {...recommend} />
              </More>
            ))}
          </>
        )}
      </MoreContents>
    </Content>
  )
}

const Details = ({ videoType, location: { pathname }, match, history }) => {
  const {
    params: { id }
  } = match
  const [backdrop, setBackdrop] = useState(null)
  const [poster, setPoster] = useState(null)
  const [genres, setGenres] = useState(null)
  const [details, setDetails] = useState(null)
  const [video, setVideo] = useState(null)
  const [recommends, setRecommends] = useState(null)
  const [seasonInfo, setSeasonInfo] = useState(null)
  const [episodeInfo, setEpisodeInfo] = useState(null)
  const [backdrops, setBackdrops] = useState([])
  const [posters, setPosters] = useState([])
  const detailContainer = useRef(null)

  const itemClickedHandler = useCallback(
    (info) => {
      if (info.episode_count) {
        const result = tvApis.getSeason(id, info.season_number)
        result.then((data) => {
          setSeasonInfo(data.data)
          const rst2 = tvApis.getEpisode(id, data.data.season_number, 1)
          rst2.then((data2) => {
            setEpisodeInfo(data2.data)
          })
        })
      } else {
        const result = tvApis.getEpisode(
          id,
          info.season_number,
          info.episode_number
        )
        result.then((data) => {
          setEpisodeInfo(data.data)
        })
      }
    },
    [id]
  )

  const onClose = useCallback(() => {
    if (pathname.includes('movie')) {
      history.push('/')
    } else {
      history.push('/tv')
    }
  }, [])

  useEffect(() => {
    const result = getCompositeInfo(pathname, id)
    result.then((infos) => {
      const [detailInfos, imageInfos, recommends] = infos

      setBackdrops([...imageInfos.data.backdrops])
      setRecommends(recommends.data && recommends.data.results)
      setDetails(detailInfos.data)
    })
    result.catch((error) => {
      console.log(error)
    })
  }, [pathname])

  useEffect(() => {
    let interval
    if (backdrops && backdrops.length) {
      let i = 0
      setBackdrop(backdrops[i])
      i++
      interval = setInterval(() => {
        setBackdrop(backdrops[i])
        i++
        if (i === backdrops.length) {
          i = 0
        }
      }, 10000)
    } else {
      setBackdrop(null)
    }
    return () => {
      clearInterval(interval)
    }
  }, [backdrops])

  useEffect(() => {
    if (details) {
      setPoster(details.poster_path)
      setGenres(details.genres.map((i) => i.name).join(' / '))
      if (
        details.videos &&
        details.videos.results &&
        details.videos.results.length
      ) {
        const [v] = details.videos.results
        setVideo(v)
      } else {
        setVideo(null)
      }
      if (videoType === 'tv') {
        const result = tvApis.getSeason(id, details.seasons[0].season_number)
        result.then((data) => {
          setSeasonInfo(data.data)
          const rst2 = tvApis.getEpisode(
            id,
            details.seasons[0].season_number,
            1
          )
          rst2.then((data2) => {
            setEpisodeInfo(data2.data)
          })
        })
      }
    } else {
      setPoster(null)
    }
    detailContainer.current.scrollTo({ top: 0 })
  }, [details])

  return (
    <Container>
      <ImageContainer ref={detailContainer}>
        <Close onClick={onClose}>✘</Close>
        {backdrop && (
          <BackDrop
            backdrop={`https://image.tmdb.org/t/p/w1280${backdrop.file_path}`}
          />
        )}
        {poster && (
          <Poster poster={`https://image.tmdb.org/t/p/w342${poster}`} />
        )}
        {videoType === 'movie' &&
          getMovieContent(details, genres, recommends, video)}
        {videoType === 'tv' &&
          getTvContent(
            details,
            genres,
            seasonInfo,
            episodeInfo,
            recommends,
            itemClickedHandler
          )}
        <Filter />
      </ImageContainer>
    </Container>
  )
}

Details.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.isRequired
    }).isRequired
  }),
  history: PropTypes.object.isRequired,
  videoType: PropTypes.string.isRequired
}

export default memo(Details)
