import React, { useRef, useEffect, useState, useCallback } from 'react'
import PropTypes from 'prop-types'
import styled, { createGlobalStyle, css } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  body,ul,div {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  a {
    text-decoration: none
  }
  ol, ul {
    list-style: none;
  }
`

const Container = styled.div`
  width: 100%;
  height: auto;
  margin: 0 1rem 1rem 0;
  cursor: pointer;
  text-shadow: none;
  color: rgba(0, 0, 0, 0.5);
  position: relative;
  @media only screen and (max-width: 1600px) {
    margin: 0.2rem 0;
  }
  z-index: ${(props) => props.zIndex};
`
const ComboboxMain = styled.div`
  ${(props) =>
    css`
      width: 100%;
      height: max-content;
      background-color: ${props.styles ? props.styles.bg : 'grey'};
      position: relative;
      display: flex;
      justify-content: space-evenly;
      align-items: center;
      border-radius: 8px;
      line-height: 1.3rem;
      padding: 0.3rem;
      div {
        width: 95%;
        font-size: 0.8rem;
        padding-right: 1rem;
        display: inline-block;
        &:last-child {
          display: flex;
          justify-content: flex-end;
          width: 10%;
        }
      }
    `}
  @media only screen and (max-width: 1600px) {
    z-index: 1;
  }
`
const ComboboxList = styled.ul`
  max-height: 8rem;
  overflow-y: auto;
  overflow-x: hidden;
  display: flex;
  margin-top: 0.2rem;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  visibility: ${(props) => (props.isShown ? 'visible' : 'hidden')};
  display: ${(props) => (props.isShown ? 'inline-block' : 'none')};
  z-index: 2;
  position: absolute;
  left: 0;
  right: 0;
  width: 100%;
  @media only screen and (max-width: 1600px) {
  }
`
const ComboboxItem = styled.li`
  background-color: ${(props) => (props.selected ? 'yellowgreen' : 'black')};
  color: ${(props) => (props.selected ? 'black' : 'white')};
  padding: 0.5rem 0.2rem;
  width: 100%;
  &:hover {
    background-color: red;
    color: white;
  }
  @media only screen and (max-width: 1600px) {
    font-size: 0.7rem;
  }
  z-index: 2;
  div {
    width: 100%;
    word-break: break-all;
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
  }
`

const Combobox = ({
  styles,
  options = null,
  prefix = null,
  itemClickedHandler = null
}) => {
  if (!options) return null

  const [isArrowUp, setArrowUp] = useState(false)
  const [selectedItem, setSelectedItem] = useState(options[0])
  const [isComboListShow, setComboListShow] = useState(false)
  const [displayName, setDisplayName] = useState(null)
  const main = useRef()
  const comboList = useRef()

  const onMouseDownHandler = useCallback((e) => {
    setArrowUp((prev) => !prev)
    setComboListShow((prev) => !prev)
  }, [])

  const clickItem = useCallback(
    (itemInfo) => (e) => {
      if (e.target.closest('li')) {
        setDisplayName(e.target.textContent)
        setSelectedItem(itemInfo)
        setArrowUp((prev) => !prev)
        setComboListShow((prev) => !prev)
        if (itemClickedHandler) {
          itemClickedHandler(itemInfo)
        }
      }
    },
    [selectedItem]
  )

  const onFocusoutHandler = useCallback((e) => {
    setArrowUp(false)
    setComboListShow(false)
  }, [])

  const keyDownHandler = useCallback(
    (e) => {
      e.preventDefault()
      if (comboList.current) {
        const t = comboList.current.querySelector('.selected')
        if (e.key === 'ArrowDown' && t.nextElementSibling) {
          setSelectedItem(options[+t.dataset.order + 1])
          setDisplayName(t.nextElementSibling.textContent)
          t.nextElementSibling.scrollIntoView({
            behavior: 'smooth',
            block: 'nearest',
            inline: 'start'
          })
        } else if (e.key === 'ArrowUp' && t.previousElementSibling) {
          setSelectedItem(options[+t.dataset.order - 1])
          setDisplayName(t.previousElementSibling.textContent)
          t.previousElementSibling.scrollIntoView({
            behavior: 'smooth',
            block: 'nearest',
            inline: 'start'
          })
        } else if (e.key === 'Enter') {
          setArrowUp(false)
          setComboListShow(false)
          if (itemClickedHandler) {
            itemClickedHandler(selectedItem)
          }
        }
      }
    },
    [selectedItem]
  )

  useEffect(() => {
    if (isComboListShow) {
      comboList.current.focus()
      const t = comboList.current.querySelector('.selected')
      if (t) {
        t.scrollIntoView({
          behavior: 'smooth',
          block: 'nearest',
          inline: 'start'
        })
      }
    }
  }, [isComboListShow])

  // useEffect(() => {
  //   if (selectedItem && itemClickedHandler) {
  //     itemClickedHandler(selectedItem)
  //   }
  // }, [selectedItem])

  useEffect(() => {
    if (!displayName) {
      if (prefix) {
        if (selectedItem) {
          if (selectedItem.name.toLowerCase().includes('episode')) {
            setDisplayName(selectedItem.name)
          } else {
            setDisplayName(`${prefix} 1: ${selectedItem.name}`)
          }
        }
      } else {
        if (selectedItem) {
          setDisplayName(selectedItem.name)
        }
      }
    }
  }, [displayName])

  useEffect(() => {
    if (options) {
      setSelectedItem(options[0])
      if (prefix) {
        if (options[0]) {
          if (options[0].name.toLowerCase().includes('episode')) {
            setDisplayName(options[0].name)
          } else {
            setDisplayName(`${prefix} 1: ${options[0].name}`)
          }
        }
      } else {
        setDisplayName(options[0].name)
      }
    }
  }, [options])
  return (
    <>
      <Container className="comboContainer" onBlur={onFocusoutHandler}>
        <ComboboxMain
          styles={styles}
          ref={main}
          isArrowUp={isArrowUp}
          onMouseDown={onMouseDownHandler}
        >
          <div>{displayName}</div>
          <div>{isArrowUp ? '▴' : '▾'}</div>
        </ComboboxMain>
        <ComboboxList
          isShown={isComboListShow}
          ref={comboList}
          onKeyDown={keyDownHandler}
          tabIndex="0"
        >
          {options.map((item, index) => (
            <ComboboxItem
              onClick={clickItem(item)}
              selected={item.id === selectedItem.id}
              className={
                item.id === selectedItem.id ? 'selected' : 'unselected'
              }
              data-order={index}
              key={item.id}
            >
              <div>
                {!item.name.toLowerCase().includes('episode') &&
                  prefix &&
                  `${prefix} ${index + 1}: `}
                {item.name}
              </div>
            </ComboboxItem>
          ))}
        </ComboboxList>
      </Container>
      <GlobalStyle />
    </>
  )
}

Combobox.propTypes = {
  styles: PropTypes.object,
  options: PropTypes.array.isRequired,
  prefix: PropTypes.string,
  itemClickedHandler: PropTypes.func
}

export default Combobox
