import React, { memo, useCallback, useContext, useState } from 'react'
import styled from 'styled-components'
import Search from './Search'
import popcorn from '../assets/images/icons8-popcorn-48.png'
import { Link } from 'react-router-dom'
import { AppContext, Type } from '../App'

const TopContainer = styled.div`
  padding: 0.5rem 0;
  background-color: red;
  position: relative;
`

const HomeContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: transform 0.3s linear;
  @media only screen and (max-width: 1600px) {
    transform: ${(props) => (props.isHovered ? 'translateX(-16rem)' : 'none')};
  }
`

const Home = styled(Link)`
  text-decoration: none;
  color: inherit;
  span {
    text-transform: uppercase;
    font-size: 2rem;
  }
  display: flex;
  justify-content: center;
  align-items: center;
  width: max-content;
`

const Top = () => {
  const { dispatch, genres } = useContext(AppContext)
  const [isHovered, setHover] = useState(null)
  const onHovered = useCallback(
    (isHovered) => {
      setHover(isHovered)
    },
    [isHovered]
  )

  const setInit = useCallback(() => {
    if (genres && genres.length) {
      dispatch({ type: Type.SET_GENRE_OPTION, genreId: genres[0].id })
    }
  }, [genres])

  return (
    <TopContainer>
      <HomeContainer isHovered={isHovered}>
        <Home to="/" onClick={setInit}>
          <span>PopCorn Time</span>
          <img src={popcorn} alt="Logo" />
        </Home>
      </HomeContainer>
      <Search onHover={onHovered} />
    </TopContainer>
  )
}

export default memo(Top)
