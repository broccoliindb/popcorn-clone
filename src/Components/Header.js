import React, { memo, useContext, useEffect, useCallback, useRef } from 'react'
import styled from 'styled-components'
import moviePng from '../assets/images/icons8-movie-48.png'
import tvPng from '../assets/images/icons8-retro-tv-48.png'
import { withRouter, Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { AppContext, Type } from '../App'
import { FAwesomeIcon, addIcon } from './FontAwesomeIcon'
import { faBars } from '@fortawesome/free-solid-svg-icons'

addIcon(faBars)
const Container = styled.div``
const HeaderTabs = styled.ul`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`
const Bar = styled.div`
  visibility: collapse;
  display: none;
  @media only screen and (max-width: 1600px) {
    visibility: visible;
    display: inline-block;
  }
  height: 100%;
  padding: 0.5rem 0 0.5rem 0.5rem;
  margin: 0;
`
const Tab = styled.li`
  padding: 0.5rem 0;
  border-bottom: 3px solid ${(props) => (props.current ? 'red' : 'transparent')};
  img {
    height: 1.5rem;
  }
  transition: border-bottom 0.3s linear;
`
const Menu = styled(Link)`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  color: inherit;
  text-decoration: none;
  padding: 0 0.5rem;
`

const Header = ({ location: { pathname }, onMenuClicked }) => {
  const { dispatch, videoType } = useContext(AppContext)
  const selectedTab = useCallback(
    (vType) => (e) => {
      if (vType !== videoType) {
        dispatch({ type: Type.SELECT_VIDEO_TYPE, videoType: vType })
      }
    },
    [videoType]
  )

  return (
    <Container>
      <HeaderTabs>
        <Bar onClick={onMenuClicked}>
          <FAwesomeIcon icon={faBars} />
        </Bar>
        <Tab current={pathname === '/'} onClick={selectedTab('movie')}>
          <Menu to="/">
            <span>movie</span>
            <img src={moviePng} alt="movie" title="movie"></img>
          </Menu>
        </Tab>
        <Tab current={pathname.includes('tv')} onClick={selectedTab('tv')}>
          <Menu to="/tv">
            <span>tv</span>
            <img src={tvPng} alt="tv" title="tv"></img>
          </Menu>
        </Tab>
      </HeaderTabs>
    </Container>
  )
}

Header.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  onMenuClicked: PropTypes.func.isRequired
}

export default withRouter(memo(Header))
