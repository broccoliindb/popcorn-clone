# Popcorn Clone with React

`Popcorn Site` clone with React

https://yong-film.netlify.app/

## 구성

크게 Movie 와 Tv Show로 탭을 구성하여 각 탭별로 메뉴옵션에 따라 조회된 리스트 나열

## 구현

- useHover를 구현하여 메뉴와 검색부분의 hover시 이벤트사용
- useIntersectionObserver를 구현하여 Infinite Scroll 구현 
- 데스크탑과 스마트폰 device 의 responsive layout 적용

## 데스크탑 화면 캡쳐

### with menu
![fullwithmenu](./covers/fullwithmenu.png)

### with search
![fullwithmenu](./covers/fullwithsearch.png)

### contents details
![fullwithmenu](./covers/fulldetails.png)

## 모바일 화면 캡쳐

### with menu
![fullwithmenu](./covers/mobile.png)

### contents details
![fullwithmenu](./covers/mobiledetail.png)



